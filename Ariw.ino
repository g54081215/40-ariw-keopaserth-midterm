#include <Servo.h>
Servo servo;
int pos;

void setup() {
  servo.attach(9);
}

void loop() {
  pos = map(analogRead(A0), 0, 1023, 0, 180);
  servo.write(pos);
  delay(15);
}